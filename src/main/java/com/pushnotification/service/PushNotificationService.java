package com.pushnotification.service;
import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.pushnotification.api.request.SendNotificationRequest;
import com.pushnotification.integration.FirebaseClient;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PushNotificationService {

    @Autowired
    private FirebaseClient firebaseClient;

    @Value("${firebase.server.key}")
    private String firebaseServerKey;

    @Async
    public CompletableFuture<String> sendNotification(final SendNotificationRequest notificationData) {
        log.info("M=sendNotificationService, status=start, request={}", notificationData);

        JSONObject notification = new JSONObject();
        notification.put("title", notificationData.getTitle());
        notification.put("body", notificationData.getBody());
        notification.put("image", notificationData.getImage());

        JSONObject body = new JSONObject();
        body.put("to", notificationData.getTarget());
        body.put("priority", notificationData.getPriority());
        body.put("notification", notification);
        body.put("data", notificationData.getData());

        log.info("M=sendNotificationService, sending data to firebase {}", body.toString());

        final String firebaseResponse = firebaseClient.sendNotification("key=" + firebaseServerKey,
                new HttpEntity<>(body.toString()));

        log.info("M=sendNotificationService, status=end, response={}", firebaseResponse);
        return CompletableFuture.completedFuture(firebaseResponse);
    }
}
