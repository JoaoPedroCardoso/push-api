package com.pushnotification.controller.exceptions;

public class CouldNotSendNotificationException extends RuntimeException {

    public CouldNotSendNotificationException(final String msg) {
        super(msg);
    }

    public CouldNotSendNotificationException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

}
