package com.pushnotification.controller;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.pushnotification.api.PushNotificationApi;
import com.pushnotification.api.request.SendNotificationRequest;
import com.pushnotification.controller.exceptions.CouldNotSendNotificationException;
import com.pushnotification.service.PushNotificationService;

@RestController
public class PushNotificationController implements PushNotificationApi {

    @Autowired
    PushNotificationService pushNotificationService;

    @Override
    public ResponseEntity<String> sendNotification(final SendNotificationRequest request) {
        final CompletableFuture<String> pushNotification = pushNotificationService.sendNotification(request);
        CompletableFuture.allOf(pushNotification).join();

        try {
            return new ResponseEntity<>(pushNotification.get(), HttpStatus.OK);
        } catch (Exception e) {
            throw new CouldNotSendNotificationException("Push Notification Fail!", e);
        }
    }
}
