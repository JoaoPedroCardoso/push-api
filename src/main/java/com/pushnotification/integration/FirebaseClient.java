package com.pushnotification.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import feign.Headers;

@FeignClient(name = "firebaseclient", url = "${firebase.client.url}")
public interface FirebaseClient {

    @RequestMapping(value = "/fcm/send", method = RequestMethod.POST)
    @Headers("Content-Type: application/json")
    String sendNotification(@RequestHeader("Authorization") String authorizationToken, HttpEntity<String> notification);

}
