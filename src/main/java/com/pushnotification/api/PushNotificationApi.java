package com.pushnotification.api;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pushnotification.api.request.SendNotificationRequest;

import io.swagger.annotations.ApiOperation;

public interface PushNotificationApi {

    @RequestMapping(value = "/send", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Send notification", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> sendNotification(@RequestBody final SendNotificationRequest request);

}
