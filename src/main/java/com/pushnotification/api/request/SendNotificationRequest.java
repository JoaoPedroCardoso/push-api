package com.pushnotification.api.request;

import javax.validation.constraints.NotBlank;

import org.json.JSONObject;

import lombok.Data;

@Data
public class SendNotificationRequest {

    @NotBlank
    private String title;

    @NotBlank
    private String body;

    @NotBlank
    private String target;

    private String image = "";

    private JSONObject data = new JSONObject();

    private String priority = "high";
}
